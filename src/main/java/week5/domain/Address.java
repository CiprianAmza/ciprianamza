package week5.domain;

public class Address {

    private final String cityName, postalCode;

    public Address(String name, String postalCode) {
        this.cityName = name;
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "cityName='" + cityName + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
