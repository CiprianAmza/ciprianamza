package week5.domain;

import java.util.Comparator;

public class MySecondComparator implements Comparator<Student> {

    public int compare(Student a, Student b) {

        if (! a.getName().equals(b.getName())) return 1;
        else if (a.getAge() != b.getAge()) return 1;
        else if (a.getYear() != b.getYear()) return 1;
        else return 0;
    }

}
