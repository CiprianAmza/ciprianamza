package week5.domain;

import java.util.ArrayList;

public class Hobby {

    private final String name;
    private final int frequency;
    private ArrayList<Address> listOfAddresses = new ArrayList<>();

    public Hobby(String name, int frequency, Address address){
        this.name = name;
        this.frequency = frequency;
        this.listOfAddresses.add(address);
    }

    public void addAddress(Address address) {
        this.listOfAddresses.add(address);
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "name='" + name + '\'' +
                ", frequency=" + frequency +
                ", listOfAddresses=" + listOfAddresses +
                '}';
    }
}
