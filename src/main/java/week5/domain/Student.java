package week5.domain;

import java.util.ArrayList;

public class Student {

    private String name;
    private int age, year;
    private boolean isClone = false;
    private ArrayList<String> hobbies = new ArrayList<>();

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student(String name, int age, int year) {
        this.name = name;
        this.age = age;
        this.year = year;
    }

    public Student(Student a) {
        this.name = a.name;
        this.age = a.age;
        this.year = a.year;
        this.isClone = true;
    }


    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", year=" + year +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getYear(){
        return year;
    }

    public int getAge() {
        return age;
    }

    public void getHobbies() {
        for (String hobby: this.hobbies) {
            System.out.println(hobby);
        }
    }

    public void addHobby(String hobby) {
        this.hobbies.add(hobby);
    }

    public void setName(String newName){
        this.name = newName;
    }

    public void setYear(int newYear) {
        this.year = newYear;
    }

    public boolean getIsClone() {
        return this.isClone;
    }


}
