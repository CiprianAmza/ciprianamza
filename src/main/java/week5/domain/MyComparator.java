package week5.domain;

import java.util.Comparator;

public class MyComparator implements Comparator<Student> {

    public int compare(Student a, Student b) {

        if (a.getName().equals(b.getName())) return 0;
        else return 1;
    }
    // No need to override equals.
}
