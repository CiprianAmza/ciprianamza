package week5;

import week5.domain.*;
import java.util.*;

/*
Some remarks about my homework:

1.      I created a class Student with 3 attributes (name, age, yearOfStudy)
    and 2 classes Comparator based on the Student class.
        The first comparator checked only the first two attributes (name and age), in order to prove that
    "new Student("Mihai", 18, 1)" and "new Student("Mihai", 18, 0)" are considered the same instance, therefore only one
    can be contained by the set.
        The second comparator took in consideration the third attribute (yearOfStudy) as well, so that both ("Mihai", 18, 0) and
    ("Mihai", 18, 1) to be considered distinct values and included in the set.

2.      I discovered a very interesting way to add an element two times in a set. If we consider the two objects:
    student1 = ("Mihai", 18, 1) and student2 = ("Mihai, 18, 0), we can simply assume that they are distinct, and we can
    add them in the set.
        BUT if we do this:
        student1.setYear(0), we will see that now we have two identical objects in the set ("Mihai, 18, 0), ("Mihai, 18, 0)

        This is because the Comparator can not check the object after being introduced in the set. A solution to
    avoid such a reaction would be not to allow the setter of any attribute included by the set OR making them final, in order to prevent
    unhappy cases like this. (But maybe some attributes should not be final)
        A second variant would be to create a `updateElement` method, which would eliminate the element from the set,
    modify it and re-add it to the set (so that a new check can be completed).
        A third way may be to have a second static set ( O(1) time complexity for checking) with all the elements ever included
    in the first set, but this would have memory problems and also adding elements into a set is O(n).

3.      I created a HashMap<Student, ArrayList<Hobby>>, in order to add and display elements of a set. I re-used the class
    Student, as I didn't think that creating a new class Person would bring anything new.


 */

public class Main {

    public static void main(String[] args) {


        //Creating 3 Student objects
        Student student1 = new Student("Mihai", 14);
        Student student2 = new Student("Marius", 14);
        Student student3 = new Student("Mihai", 14, 1);


        /* Comparator<String> comp = (String o1, String o2) -> (o1.compareTo(o2));  -> another way of implementing the comparator,
                                                                                    but I considered that creating a special class was a better approach */


        System.out.println("First comparator: ");
        //Using MyComparator, where I only compare the name and the age of the students.
        TreeSet<Student> mySet = new TreeSet<>(new MyComparator());

        mySet.add(student1);
        System.out.println("It will print TRUE if the student1 is in mySet (as expected): ");
        System.out.println(mySet.contains(student1)); //Student1 is in mySet

        mySet.add(student2);
        System.out.println("It will print TRUE if the student3 is in mySet (as expected): ");
        System.out.println(mySet.contains(student3)); /*Student 3 is also in mySet,
                                                    because it has the same name and age as the student1.
                                                    His third parameter is not checked by this comparator.
                                                    Therefore, student3 can't be added to the class */

        System.out.println("Printing the set before adding student3: ");
        System.out.println(mySet);
        mySet.add(student3); //This will not produce any change, as the comparator considers student1 == student3
        System.out.println("Printing the set after adding student3. NORMALLY NO CHANGE should be produced: ");
        System.out.println(mySet);
        System.out.println(" ");


        //crating a second set, with a second comparator (now a third parameter is taken into consideration)
        TreeSet<Student> myNewSet = new TreeSet<>(new MySecondComparator());

        myNewSet.add(student1);
        System.out.println("This time it should print FALSE - student3 is not in the set: ");
        System.out.println(myNewSet.contains(student3)); //here it will print false, as we now compare all three attributes

        myNewSet.add(student2);
        myNewSet.add(student3);

        System.out.println("Displaying the new set with 3 objects.");
        for (Student currStudent : myNewSet) {
            System.out.println(currStudent);
        }
        System.out.println(" ");


        //Creating some Address and Hobby instances
        Address address1 = new Address("Berlin", "134002");
        Address address2 = new Address("Moscow", "998441");
        Address address3 = new Address("Paris", "443441");

        Hobby hobby1 = new Hobby("swimming", 100, address1);
        Hobby hobby2 = new Hobby("football", 80, address3);
        Hobby hobby3 = new Hobby("sky", 90, address2);
        Hobby hobby4 = new Hobby("dance", 30, address1);
        hobby4.addAddress(address2);

        //Defining of a map (dictionary) and a set of Hobby instances
        HashMap<Student, ArrayList<Hobby>> myMap = new HashMap<>();

        ArrayList<Hobby> setOfHobbies1 = new ArrayList<>();
        setOfHobbies1.add(hobby1);
        setOfHobbies1.add(hobby3);
        myMap.put(student1, setOfHobbies1);


        //second set of hobbies
        ArrayList<Hobby> setOfHobbies2 = new ArrayList<>();
        setOfHobbies2.add(hobby1);
        setOfHobbies2.add(hobby2);
        setOfHobbies2.add(hobby4);
        myMap.put(student2, setOfHobbies2);

        //third student with the same set of hobbies as the second student
        myMap.put(student3, setOfHobbies2);

        System.out.println("Printing the map: ");
        for (HashMap.Entry<Student, ArrayList<Hobby>> currStudent : myMap.entrySet()) {
            System.out.println(currStudent);
        }

        System.out.println(" ");
        student1.setName("Ciprian");

        for (Student currStudent : myNewSet) {
            System.out.println(currStudent);
        }
        System.out.println(" ");


        student1.setYear(1);
        student1.setName("Mihai");
        System.out.println("Interesting observation!");
        for (Student currStudent : myNewSet) {
            System.out.println(currStudent);
        }
        /* Now I actually have the same element twice in a set. I assume the way to go is
        to not allow the setter to any attribute that is contained into a set.
         */


        Student myStudent = new Student("Ciprian", 25, 1);
        Student cloned = new Student(myStudent);

        System.out.println(cloned.getIsClone());
        System.out.println(myStudent == cloned);

        mySet.add(myStudent);
        System.out.println(mySet.contains(myStudent));
        System.out.println(mySet.contains(cloned));

        List<String> l = new ArrayList<>();
        l.add("d");
        l.add("sss");

        String c = String.join(" ", l);
        System.out.println(c);



        String m1 = "abcdefghijklmnopqrstuvwxyz";
        List<String> l1 = Arrays.asList(m1.split(""));

        Collections.shuffle(l1);
        for (String el: l1) {
            System.out.println(el);
        }


    }
}