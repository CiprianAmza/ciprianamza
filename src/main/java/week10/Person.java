package week10;

public class Person {

    public enum Gender {
        F, M;
    }
    private Gender gender;
    private String firstName, lastName;
    private int age;
    private boolean isMarried;

    Person() {}
    Person(String firstName, String lastName, int age, boolean isMarried, String gender) {
        this.firstName  = firstName;
        this.lastName   = lastName;
        this.age        = age;
        this.isMarried  = isMarried;
        this.gender     = gender.equals("male") ? Gender.M : Gender.F;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAge() {
        return age;
    }

    public int getMarriedStatus() {
        return this.isMarried == true ? 1 : 0;
    }
    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isMarried=" + isMarried +
                '}';
    }
}
