package week10;

import java.util.Comparator;

public class MyComparator implements Comparator<User>{

    public int compare(User a, User b) {
        String name1 = a.getLastName(), name2 = b.getLastName();
        if (name1.equals(name2)) return 0;
        return name1.compareTo(name2);
    }

}