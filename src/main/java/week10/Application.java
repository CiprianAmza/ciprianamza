package week10;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Application {

    public TreeSet<User> users = new TreeSet(new MyComparator());
    int givenMonth = 0;

    Application() {}
    Application(int month) {
        this.givenMonth = month;
    }

    public String getGoodUser() {
        List<String> answer = new ArrayList<>();
        for (User el: this.users) {
            answer.add(el.toString());
        }
        return String.join("\n", answer);
    }

    public void writeFile(String contentOfFile){
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("src/main/java/week9/newFile.txt"), "utf-8"))) {
            writer.write(contentOfFile);
        }
        catch (Exception e) {

        }
    }

    public int getGivenMonth() {
        return givenMonth;
    }
}
