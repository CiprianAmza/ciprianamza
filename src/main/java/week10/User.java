package week10;

import java.time.LocalDate;

public class User {

    private String firstName, lastName;
    private LocalDate dateOfBirth;

    User() {}

    User(String firstName, String lastName, String dateOfBirth) {
        this.firstName      = firstName;
        this.lastName       = lastName;
        this.dateOfBirth    = LocalDate.parse(dateOfBirth);
    }

    public int getYearOfBirth(){
        return dateOfBirth.getYear();
    }

    public int getMonthOfBirth() {
        return dateOfBirth.getMonthValue();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return firstName + ' ' + lastName;
    }
}
