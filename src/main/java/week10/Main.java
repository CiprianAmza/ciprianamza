package week10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        // month July
        Application currentApplication = new Application(7);

        try {
            Stream<String> stream = Files.lines(Paths.get("src/main/java/week9/fisier.txt"));
            List<String> lst = stream.collect(Collectors.toList());
            for (String line : lst) {
                String[] elementsOfLine = line.split(" ");
                User user = new User(elementsOfLine[0], elementsOfLine[1], elementsOfLine[2]);
                if (user.getMonthOfBirth() == currentApplication.getGivenMonth()) {
                    currentApplication.users.add(user);
                }
            }
            System.out.println(currentApplication.users);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //writing a new file with only the persons borned in the given month
        String validUsers = currentApplication.getGoodUser();
        currentApplication.writeFile(validUsers);

    /*
    1) Write 5 different instances of that object.
    Add the instances to a list.
     */
        Person  person1 = new Person("Ciprian", "Amza", 25, false, "male"),
                person2 = new Person("Adrian", "Antoni", 31, false, "male"),
                person3 = new Person("Maria", "Antonescu", 22, true, "female"),
                person4 = new Person("Marius", "Antonescu", 23, true, "male"),
                person5 = new Person("Andrei", "Artean", 22, false, "male");

        List<Person> listOfPersons = Arrays.asList(person1, person2, person3, person4, person5);


    /*
    2) Find the elements containing the letter "a" that start with "M" and print them out.

    Add the instances to a set.
     */
        listOfPersons.stream().filter(p -> !p.getFirstName().isEmpty() &&
                p.getFirstName().charAt(0) == 'M' &&
                p.getFirstName().contains("a")).forEach(System.out::println);


    /*
    3) Find the "min" using a custom comparing criteria of choice
     */
        int minValue = listOfPersons.stream().mapToInt(Person::getAge).min().orElse(0);
        System.out.println(minValue);


    /*
    4) Generate 5 random Strings and add them to a Set. Find the "max" (while explaining as Javadoc how comparing Strings works)
     Strings are compared through `compareTo` method. Each letter is compared, in a zip-format, for example:
     Ana, Anb -> A and A are compared, then n and n, then a and b. When the first difference occurs, they are compared based on
     their ascii value.
     */
        HashSet<String> names = new HashSet<>();
        Random rand = new Random();
        List<String> vowels = Arrays.asList("a", "e", "i", "o", "u");
        List<String> starts = Arrays.asList("sta", "bi", "ga", "kru", "vru", "kro", "kir", "pi", "ar", "ur", "er", "ev");
        List<String> endings = Arrays.asList("noski", "oski", "utki", "olis", "uri", "inov", "urov", "illi", "illo");

        for (int i = 0; i < 5; i++) {
            int firstNumber  = rand.nextInt(starts.size()-1),
                secondNumber = rand.nextInt(vowels.size()-1),
                thirdNumber  = rand.nextInt(endings.size()-1);
            String newName = starts.get(firstNumber) + vowels.get(secondNumber) + endings.get(thirdNumber);
            names.add((newName.charAt(0)+"").toUpperCase(Locale.ROOT) + newName.substring(1));
        }

        names.forEach(System.out::println);

    /*
    5) Generate a random number of Integers and then count them. "Map" the exponential to the numbers and then print them out.
     */

        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < rand.nextInt(50); i++) {
            numbers.add(rand.nextInt(100));
        }
        System.out.println(numbers.stream().count());

    /*
    6) Create a map of "n" (K,V) elements and print "how many" elements have value over 10 (the key is of your choice)
     */
        Map<Integer, Integer> newMap = new HashMap<>();
        for (int i = 0; i < rand.nextInt(5, 500); i++) {
            int a = rand.nextInt(i+5),
                b = rand.nextInt(i+5);
            newMap.put(a, b);
        }
        Map<Integer, Integer> filteredMap = newMap.entrySet().stream().filter(x-> x.getValue() > 10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        System.out.println(filteredMap.size());


     /*
     Sort the above Set<String> (used for max) in reverse order
      */

        listOfPersons = listOfPersons.stream().sorted((x, y) -> Integer.compare(y.getAge(), x.getAge())).collect(Collectors.toList());
        System.out.println(listOfPersons);



     /*
     8) Sort the above List of custom objects (used for filter) in an order you consider
      */
        listOfPersons.stream().sorted();


    /*
    9) Check if any of your instances "match" a condition based on an Integer field (of your choice). What does it return ? Print it out.
     */

        boolean check = listOfPersons.stream().filter(p -> p.getAge() > 24).collect(Collectors.toList()).size() > 0;
        System.out.println(check);

    /*
    10) What does Optional represent ? Explain through an example on your custom object

-> Wrap an existing instance

-> Wrap a null

-> Check value using ifPresent or isPresent
     */
        Optional<String> f;
        f = Optional.of("foo"); // attribute has non-null value
        f = Optional.empty();   // attribute is present, but it is null
        f = null;               // attribute is not present


    /*
    11) Fastest way to find the shortest String in a List (take the 5 random Strings generated above and find the shortest one).
     */

        String shortest = names.stream().sorted((b, a) -> a.length() > b.length() ? -1 : 1).findFirst().get();
        System.out.println(shortest);
    }

}
