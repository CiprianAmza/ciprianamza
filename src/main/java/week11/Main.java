package week11;
import java.util.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {


        FestivalGate poarta = new FestivalGate();
        Random r = new Random();

        for (int i = 0; i < 100; i++) {
            TicketType ticket = new TicketType();
            String ticketType = "";
            int valoareAleatorie = r.nextInt(5);

            if (valoareAleatorie == 0) ticketType = "VIP";
            else if (valoareAleatorie == 1) ticketType = "FULL-VIP";
            else if (valoareAleatorie == 2) ticketType = "NORMAL";
        }















        Queue<TicketType> tickets = new ArrayDeque<>();
        List<FestivalGate> gates = Arrays.asList(new FestivalGate(), new FestivalGate(), new FestivalGate());
        Random rand = new Random();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(gates.get(0));



        while (1 > 0 ) {
            for (int i = 0; i < 200; i++) {
                TicketType ticketType = new TicketType(rand.nextInt(5));
                tickets.add(ticketType);
            }

            for (TicketType ticket : tickets) {
                int nrGate = rand.nextInt(3);
                FestivalGate gate = gates.get(nrGate);
                FestivalAtendeeThread attendee1 = new FestivalAtendeeThread(ticket, gate);
                attendee1.run(nrGate);
                Thread.sleep(5);
                statisticsThread.allTickets.put("total", statisticsThread.allTickets.get("total") + 1);
                if (statisticsThread.allTickets.containsKey(ticket.getTicketType())) {
                    statisticsThread.allTickets.put(ticket.getTicketType(), statisticsThread.allTickets.get(ticket.getTicketType()) + 1);
                } else {
                    statisticsThread.allTickets.put(ticket.getTicketType(), 1);
                }
            }
            System.out.println("PAUZA DE MASA 5 secunde!");
            System.out.println(statisticsThread.allTickets);
            Thread.sleep(5000);

        }
    }

}
