package week11;

import java.util.Arrays;
import java.util.List;

public class TicketType {

    public enum Type {
        FULL, FULL_VIP, FREE_PASS, ONE_DAY, ONE_DAY_VIP
    }


    private Type ticketType;
    private List<Type> types = Arrays.asList(Type.FULL, Type.FULL_VIP, Type.FREE_PASS, Type.ONE_DAY, Type.ONE_DAY_VIP);

    TicketType(){}

    TicketType(int ticketType) {
        this.ticketType = types.get(ticketType);
    }

    public String getTicketType() {
        return ticketType.toString();
    }

    @Override
    public String toString() {
        return "ticketType=" + ticketType;
    }
}
