package week11;

public class FestivalAtendeeThread extends Thread {

    private TicketType ticketType;
    private FestivalGate festivalGate;

    FestivalAtendeeThread() {}

    FestivalAtendeeThread(TicketType ticketType, FestivalGate gate) {
        this.ticketType   = ticketType;
        this.festivalGate = gate;
    }

    public void run(int nrGate) {
        System.out.println("Gate " + (nrGate + 1) + ": " + this.ticketType);
    }
}
