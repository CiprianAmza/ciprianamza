package week12;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        try {
            // Connecting to postgresql
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "260795");
            Statement st = conn.createStatement();

            //Creating table accommodation
            PreparedStatement accommodation = conn.prepareStatement("CREATE TABLE IF NOT EXISTS accommodation(id SERIAL NOT NULL PRIMARY KEY, type varchar(32) NOT NULL UNIQUE, bed_type varchar(32), max_guests INT, description varchar(512))");
            accommodation.executeUpdate();
            accommodation.close();

            //Creating table room_fair
            PreparedStatement room_fair = conn.prepareStatement("CREATE TABLE IF NOT EXISTS room_fair(id SERIAL NOT NULL PRIMARY KEY, value serial, season varchar(32))");
            room_fair.executeUpdate();
            room_fair.close();

            //Creating accommodation_room_fair_relation
            PreparedStatement accommodationRoomFairRelation = conn.prepareStatement("CREATE TABLE IF NOT EXISTS accommodation_room_fair_relation(id SERIAL NOT NULL PRIMARY KEY, accommodation_id INT FOREIGN KEY(accommodation), room_fair_id INT FOREIGN KEY(room_fair))");
            accommodationRoomFairRelation.executeUpdate();
            accommodationRoomFairRelation.close();

            ResultSet rs = st.executeQuery("INSERT INTO accommodation \n" +"VALUES (12, 'someType', 'bedType declaration', 17, 'descrierea mea');");

            rs = st.executeQuery("Select * From accommodation;");

            while (rs.next())
            {
                System.out.print(rs.getString(1) + " ");
                System.out.println(rs.getString(2));
            }
            rs.close();
            st.close(); } catch (SQLException e) {

        }
    }
}
