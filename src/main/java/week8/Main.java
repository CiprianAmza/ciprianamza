package week8;

import week8.domain.Competitor;
import week8.domain.MyComparator;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class Main {

    public static void main(String[] args) {

        Competitor k = new Competitor();
        TreeSet<Competitor> competitors = new TreeSet<>(new MyComparator());

        try (Stream<String> stream = Files.lines(Paths.get("src/main/java/week8/domain/competitionDates.csv"))) {
            List<String> lst = stream.collect(Collectors.toList());
            for (String line: lst) {
                String[] elementsOfLine = line.split(",");
                String penaltyTime = elementsOfLine[4] + " " + elementsOfLine[5] + " " + elementsOfLine[6];
                Competitor competitor = new Competitor(elementsOfLine[0], elementsOfLine[1], elementsOfLine[2], penaltyTime, elementsOfLine[3]);
                competitors.add(competitor);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int place = 0;
        String[] places = {"Winner     ", "Runner-up  ", "Third place"};
        for (Competitor c: competitors) {
            System.out.println(places[place] + c);
            place++;
            if (place == 3) break;
        }
    }

}
