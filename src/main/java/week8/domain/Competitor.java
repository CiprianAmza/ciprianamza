package week8.domain;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Competitor {
    //public List<T> k = Arrays.asList(1, 2, 3);
    public enum Nationality {
        DE, SK, ROU, CZ, UK, ENG, FRA
    }
    private String firstName, lastName, time, totalTime;
    private Nationality nationality;
    private int totalPenaltyTime, completeTime;
    private int ID;

    public Competitor(){};

    public Competitor(String ID, String fullName, String nationality, String shooting, String time) {

        String[] transformFullName = fullName.split(" ");
        this.ID               = Integer.parseInt(ID);
        this.firstName        = transformFullName[0];
        this.lastName         = transformFullName[1];
        this.nationality      = Nationality.valueOf(nationality);
        this.time             = time;
        this.totalPenaltyTime = calculatePenaltyTime(shooting);
        this.completeTime     = this.totalPenaltyTime + this.transformTimeInSeconds(time);
        this.totalTime        = this.transformTimeToStringFormat();
    }


    public int calculatePenaltyTime(String shootings) {

        String[] allShootings = shootings.split(" ");
        int totalMisses = 0;
        for (int shoot = 0; shoot < 3; shoot++) {
            String currentShooting = allShootings[shoot];
            for (int index = 0; index < currentShooting.length(); index++) {
                if (currentShooting.charAt(index) == 'o') totalMisses++;
            }
        }
        return totalMisses * 10;

    }

    public int transformTimeInSeconds(String timeToBeTransformed) {

        int seconds = 0;
        String[] parsedTime = timeToBeTransformed.split(":");

        for (int i = 0; i < parsedTime.length; i++) { // proud of this idea -> works for hours as well
            seconds += Integer.parseInt(parsedTime[i]) * Math.pow(60, parsedTime.length - i - 1);
        }

        return seconds;
    }

    public int getCompleteTime() {
        return completeTime;
    }

    public String transformTimeToStringFormat() {

        int completedTime = this.completeTime;
        String answer = "";

        int hours = (int) completeTime / 3600;
        completedTime -= hours * 3600;

        int minutes = (int) completedTime / 60;
        completedTime -= minutes * 60;

        int seconds = completedTime;

        return Integer.toString(hours) + ":" + Integer.toString(minutes) + ":" + Integer.toString(seconds);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTime() {
        return time;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public int getTotalPenaltyTime() {
        return totalPenaltyTime;
    }

    public int getID() {
        return ID;
    }

    @Override
    public String toString() {
        return " - " + this.getFirstName() + " " + this.getLastName() + " " + this.getTotalTime() + " (" + this.getTime() + " + " + this.getTotalPenaltyTime() + ") " + this.nationality;
    }
}
