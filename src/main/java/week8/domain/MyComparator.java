package week8.domain;

import java.util.Comparator;

public class MyComparator implements Comparator<Competitor> {

    public int compare(Competitor a, Competitor b) {

        int timeA = a.getCompleteTime(), timeB = b.getCompleteTime();

        if (timeA != timeB) {
            if (timeA > timeB) return 1; // a > b
            return -1; // a < b
        }
        return 0; // a == b

    }
}
