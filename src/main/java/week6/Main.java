package week6;

import week6.domain.Calculator;

/*
    I created a Calculator class, which allows the user to create an object of this instance and is able to do the following
operations: adding or subtracting to a current value, modifying or resetting the value or even calculating and parsing
a string expression.

    Let's assume a simple Calculator instance: Calculator myCalculator = new Calculator();

    1. The calculator dispose of the method `displayValue(unit)`, where unit, if not selected, would be "mm" by default.
The initial value of the calculator is 0.

    2. Another important method is "setValue(unit)", where unit by default is "mm". For example:
        myCalculator.displayValue(); // 0 mm
        myCalculator.setValue(100, "m")
        myCalculator.displayValue("m"); // 100 m

    3. The user can add or subtract values to the current value and even choose the unit of measure. In order to do this,
the methods 'add(value, unit)' and 'subtract(value, unit)' are available. If not selected, `unit` will be "mm" by default.

    4. The user can choose to reset the current displayed value back to 0 by using the method "clearValue".
    Let's suppose this case:
        myCalculator.displayValue(); // 0 mm
        myCalculator.add(1, "m");
        myCalculator.displayValue("dm"); // 10 dm
        myCalculator.clearValue();
        myCalculator.displayValue(); // 0 mm

    5. The last method of the Calculator (and probably the most important one) is the `calculate(expression, unit)`
method. This method allows the user to send a string expression of the type "10m+3km-100m" to the calculator and to receive
the answer of this. The user can as well choose the unit, which by default (if not selected) is "mm".
        Some very important aspects:
            - the parser ignores the spaces: "-123km-11m+3m" and " -1 2 3 k m - 11m             +3m" are both valid and
            will return the same result.
            - the user can write both "3.5m + 3.5m" or "3,5m + 3.5m". "," and "." are synonyms in this context.
            - expression can start with a negative sign "-3m + 5m".
            - the method will check if the expression is valid and will return a "Please enter a valid unit of measure"
             message if a unit of measure outside of ("mm", "cm", "dm", "m" or "km") will be used.
            - if the expression doesn't start with a sign or with a digit, an "Invalid expression!" message will be displayed.
            - the parser can also be used to transform from a unit to another:
            myCalculator.calculate("1m", "dm") // 10 dm
            myCalculator.calculate("1000mm", "m") // 1 m

        The current `displayValue()` value will be replaced with the result of the `calculate(expression, unit)` method.
        If the expression is not a valid one, 0.0 will be returned.
        More tests have been created in order to check the validity of my methods.



 */
public class Main {
    public static void main(String[] args) {

        Calculator myCalculator = new Calculator();
        myCalculator.displayValue();

        myCalculator.setValue(100);
        myCalculator.displayValue();

        myCalculator.setValue(100, "dm");
        myCalculator.displayValue("m");
        System.out.println(" ");

        //Checking the `calculate` method with spaces:
        System.out.println(myCalculator.calculate("3  m m      -12cm+   1,2km "));

        //Checking the `calculate` method with an invalid unit of measure
        System.out.println(myCalculator.calculate("1000000dmm+1m"));

        //Checking the `calculate` method with an invalid format of the expression
        System.out.println(myCalculator.calculate("mm"));

        myCalculator.displayValue();
        System.out.println(myCalculator.subtract(323.3));
        System.out.println(myCalculator.subtract(323.3));
        System.out.println("Current displayed value is: " + myCalculator.getCurrentValue());
        myCalculator.clearValue();
        System.out.println("Current displayed value is: " + myCalculator.getCurrentValue());


        System.out.println(" ");
        System.out.println("Current value: " + myCalculator.getCurrentValue());
        System.out.println("Adding " + myCalculator.add(57) + " to the current value");
        System.out.println("New current value: " + myCalculator.getCurrentValue());
        System.out.println("Subtracting 57 from the current value will give: " + myCalculator.subtract(57));
        System.out.println("New value is: " + myCalculator.getCurrentValue());
        System.out.println(" ");


        System.out.println("New value is " + myCalculator.calculate("57m+  3 4 dm"));
        System.out.println("After subtracting 60399 we have: " + myCalculator.subtract(60399));
        System.out.println(" ");

        System.out.println("1 meter in decimeters is: " + myCalculator.calculate("1m", "dm") + " dm."); //1 meter in decimeters =10 dm
        System.out.println("1 meter in kilometers is: " + myCalculator.calculate("1m", "km") + " km."); //1 meter in kilometers = 0.001 km

        System.out.println("New expression: ");
        System.out.println(myCalculator.calculate("1m + 2m + 3m - 60dm + 1 km", "km"));
    }
}
