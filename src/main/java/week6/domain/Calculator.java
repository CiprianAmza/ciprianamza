package week6.domain;
import java.util.*;

public class Calculator {

    private final HashSet<String>
            digits = new HashSet<>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".")),
            signs = new HashSet<>(Arrays.asList("+", "-")),
            acceptableUnits = new HashSet<>(Arrays.asList("mm", "cm", "dm", "m", "km"));

    private final Map<String, Integer> values =
            Map.of("mm", 1,
                    "cm", 10,
                    "dm", 100,
                    "m", 1000,
                    "km", 1000000);

    private double currentValue = 0; //the value displayed on the calculator after each method

    public Calculator() {} //Constructor

    public double calculate(String expression, String unit) {  //Most important method of the code

        //if the expression doesn't start with a digit, the format is treated as invalid
        try {
            Integer.parseInt(expression.charAt(0) + "");
        } catch(NumberFormatException e) {
            System.out.println("Invalid expression!");
            return 0;
        }

        //In case the user selected "," with the sense of ".", we have to deal with it
        expression = expression.replace(",", ".");


        double totalSum = 0; //the final result
        boolean sign = true; // true for positive(+), false for negative(-)
        StringBuilder currentNumber = new StringBuilder();
        String currentType = "";
        String firstElement = expression.charAt(0) + "";

        /* if the expression doesn't start with a "-" or with a "+", we will add "+" by default,
        so that we know it's a positive expression */
        if (!signs.contains(firstElement)) expression = "+" + expression;

        for (int i = 0; i < expression.length(); i++) { //we will iterate the whole expression

            String element = expression.charAt(i) + ""; //we will store the current element into a string

            if (element.equals(" ")) continue; //ignoring the spaces
            else if (digits.contains(element)) {
                currentNumber.append(element); //if the element is a digit, we will add it to the currentNumber variable
            }
            else if (signs.contains(element) || i == expression.length() - 1) {
                /* in this else we have two possibilities:
                    1. we deal with a sign "+" or "-", so we have to modify the sign variable
                    2. we deal with the very last element of the string
                 */
                if (i == expression.length() - 1) currentType += element; //if this is the last element, then we add the element to the currentType

                if (i == 0) { //in the other case, we deal with the sign
                    sign = element.equals("+"); // true if positive, false if negative
                    continue; /* we will stop here if we deal with the very first index, because:
                                if we have an expression like "1km - 2km", we add 1 only when we meet the "-" */

                }

                double newNumber = Double.parseDouble(currentNumber.toString()); //parsing currentNumber to double

                if (! acceptableUnits.contains(currentType)) { //Checking if the currentType is an acceptable unit
                    System.out.println("Please enter a valid unit of measure.\n Acceptable units are: mm, cm, dm, m and km!");
                    return 0;
                }

                int valueToMultiply = values.get(currentType); //the difference between the currentType and "mm"

                if (! sign) newNumber *= -1; //making the number negative if the case
                newNumber *= valueToMultiply; //transforming number in "mm"
                totalSum += newNumber; //adding the currentNumber in the totalSum
                currentNumber = new StringBuilder(currentType = ""); //resetting currentNumber and currentType
                sign = element.equals("+"); //changing the old sign with the current one

            }
            else currentType += element; //the last case is that we deal with a part of the unit (a letter of "m", "k", "d")
        }

        totalSum = totalSum / values.get(unit); //transforming "mm" to the wanted unit

        this.currentValue = totalSum; //storing the value in the instance

        return totalSum; //returning the value
    }

    public double calculate(String expression) { //Overloading the calculate method, so that "mm" is the default unit
        return calculate(expression,"mm");
    }

    public double subtract(double value, String unit) {

        try {
            this.currentValue -= value * values.get(unit);
        }
        catch (Throwable t){
            System.out.println("Not a valid unit!");
            return 0;
        }
        return this.currentValue;
    }

    public double subtract(double value) { //Overloading subtract method, so that "mm" is the default unit
        return this.subtract(value, "mm");
    }

    public double add(double value, String unit) {
        try {
            this.currentValue += value * values.get(unit);
        }
        catch (Throwable t) {
            System.out.println("Not a valid unit!");
            return 0;
        }
        return this.currentValue;
    }

    public double add(double value) { //Overloading add method, so that "mm" is the default unit
        return this.add(value, "mm");
    }

    public void clearValue() { //Setter only to 0
        this.currentValue = 0;
    }

    public void setValue(double value, String unit) {
        try {
            this.currentValue = value * this.values.get(unit);
        }
        catch (Throwable t) {
            System.out.println("Invalid unit selected");
        }
    }

    public void setValue(double value) {
        this.setValue(value, "mm");
    }

    public void displayValue(String unit) {
        System.out.println("Current value: " + this.currentValue / this.values.get(unit) + " " + unit);
    }

    public void displayValue() {
        this.displayValue("mm");
    }

    public double getCurrentValue() { //Getter
        return this.currentValue;
    }
}
