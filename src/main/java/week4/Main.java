package week4;

import week4.domain.*;

public class Main {

    public static void main(String[] args) {

        //Car vehicle1 = new Dacia(27, "a3132df312@12"); //Cannot be instantiated
        Car vehicle1 = new Logan(27, "a3132df312@12");
        vehicle1.setAvailableFuel(30);
        vehicle1.setTireSize(16);
        System.out.println(vehicle1);
        vehicle1.start();

        vehicle1.shiftGear(1);
        vehicle1.drive(0.01);

        vehicle1.shiftGear(2);
        vehicle1.drive(2);

        vehicle1.stop();

        double availableFuel = vehicle1.getAvailableFuel();
        double fuelConsumedPer100Km = vehicle1.getAverageFuelConsumption();

        System.out.println(availableFuel);
        System.out.println(fuelConsumedPer100Km);

        Car mercedesAMG = new MercedesAMG(30, "afas3312@121!");
        mercedesAMG.setTireSize(17);
        mercedesAMG.setAvailableFuel(25);

        Car mercedesBenz = new MercedesBenz(25, "f@af#12132@@!!32");
        mercedesBenz.setTireSize(18);
        mercedesBenz.setAvailableFuel(50);

        Car duster = new Duster(20, "f#@!$%df2312#!!");
        duster.setAvailableFuel(30);
        duster.setTireSize(15);


        System.out.println(duster.getAvailableFuel());
        duster.drive(1);
        duster.shiftGear(2);
        duster.drive(1);
        duster.shiftGear(3);
        System.out.println(duster.getAvailableFuel());
        duster.drive(1);
        duster.shiftGear(4);
        duster.drive(1);
        duster.shiftGear(5);
        duster.drive(1);
        System.out.print("Fuel available: ");
        System.out.println(duster.getAvailableFuel());

        System.out.print("Average fuel consumption: ");
        System.out.println(duster.getAverageFuelConsumption());

        System.out.print("Total Km driven: ");
        System.out.println(duster.getTotalKmDriven());

        System.out.print("Current Km driven: ");
        System.out.println(duster.getCurrentKmDriven());

        duster.stop();

        System.out.print("Total Km driven: ");
        System.out.println(duster.getTotalKmDriven());

        System.out.print("Current Km driven: ");
        System.out.println(duster.getCurrentKmDriven());

    }
}
