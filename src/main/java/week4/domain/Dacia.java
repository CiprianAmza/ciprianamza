package week4.domain;

public abstract class Dacia extends Car {

    protected double[] fuelConsumption;
    protected int availableFuel, tireSize;
    protected String chassisNumber;
    protected final String fuelType;
    protected final int fuelTankSize = 1000; //1000 liters default size for Dacia
    protected double consumptionPer100Km = 4.7;

    Dacia(int availableFuel, String chassisNumber, int tireSize, String fuelType, double [] fuelConsumption) {
        super(availableFuel, chassisNumber, tireSize, fuelType, fuelConsumption);

        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.chassisNumber = chassisNumber;
        this.fuelType = fuelType;
        this.fuelConsumption = fuelConsumption;


    }
}
