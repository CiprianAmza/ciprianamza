package week4.domain;

public abstract class Mercedes extends Car {

    private double[] fuelConsumption;
    private int availableFuel, tireSize;
    private String chassisNumber;
    private final String fuelType;
    private final int fuelTankSize = 1000; //1000 liters default size for Dacia
    private double consumptionPer100Km = 4.7;

    Mercedes(int availableFuel, String chassisNumber, int tireSize, String fuelType, double [] fuelConsumption) {
        super(availableFuel, chassisNumber, tireSize, fuelType, fuelConsumption);
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.chassisNumber = chassisNumber;
        this.fuelType = fuelType;
        this.fuelConsumption = fuelConsumption;


    }
}
