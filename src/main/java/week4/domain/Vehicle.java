package week4.domain;

public interface Vehicle {


    public void start();

    public void stop();

    public void drive(double nrKm);
}
