package week4.domain;


public class MercedesAMG extends Mercedes {


    private static final double [] fuelConsumption = {5.3, 5.8, 6.2, 6.9, 7.4, 7.8};
    private int availableFuel;
    private final String fuelType = "Diesel"; //Logan will use Diesel
    private int tireSize = 18; //default value
    private String chassisNumber;

    public MercedesAMG(int availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber, 18, "Diesel", fuelConsumption);
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }
}
