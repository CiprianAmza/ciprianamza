package week4.domain;

public class Duster extends Dacia {

    protected static final double [] fuelConsumption = {4.7, 4.9, 5.4, 5.9, 6.1, 7.0}; //fuelConsumption, where gear 1 corresponds to index 0
    protected int availableFuel;
    protected final String fuelType = "Petrol";
    protected int tireSize = 16; //default value
    protected String chassisNumber;

    public Duster(int availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber, 16, "Petrol", fuelConsumption);
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }

}
