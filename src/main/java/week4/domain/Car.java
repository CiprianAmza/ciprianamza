package week4.domain;

import java.util.ArrayList;

public class Car implements Vehicle {

    protected final double[] fuelConsumption;
    protected ArrayList<Double> fuelConsumedMedian = new ArrayList();
    protected final int gears = 6;
    protected int currentGear = 0; //default gear, when the car is stopped
    protected double totalKmDriven = 0, averageFuelConsumption, currentKmDriven = 0;
    protected int availableFuel, tireSize;
    final protected String chassisNumber, fuelType;

    Car(int availableFuel, String chassisNumber, int tireSize, String fuelType, double [] fuelConsumption) {
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
        this.tireSize = tireSize;
        this.fuelType = fuelType;
        this.fuelConsumption = fuelConsumption;

    }

    @Override
    public String toString() {
        return "Car{" +
                "gears=" + gears +
                ", currentGear=" + currentGear +
                ", totalKmDriven=" + totalKmDriven +
                ", availableFuel=" + availableFuel +
                ", AverageFuelConsumption=" + averageFuelConsumption +
                ", tireSize=" + tireSize +
                ", chassisNumber='" + chassisNumber + '\'' +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }

    public void start() {
        System.out.println("Car starts...");
    }

    public void stop() {
        this.currentGear = 0;
        System.out.println("Car stops...");
        this.averageFuelConsumption = 0; //reset when car stops
        this.currentKmDriven = 0;
        this.fuelConsumedMedian.clear();

    }

    public void drive(double nrKm) {
        if (this.availableFuel >= nrKm * this.averageFuelConsumption) {
            this.totalKmDriven += nrKm;
            this.currentKmDriven += nrKm;
            this.availableFuel -= nrKm * averageFuelConsumption;
        }
        else {
            this.availableFuel = 0;
            System.out.println("Not enough fuel");
        }

    }

    public void shiftGear(int newGear) {

        if (newGear <= this.gears && newGear >= 0) { //using only an available gear
            this.currentGear = newGear;
            averageFuelConsumption = fuelConsumption[this.currentGear] + (this.tireSize / 4);
            this.fuelConsumedMedian.add(averageFuelConsumption);
        }
    }

    public int getAvailableFuel() {
        return availableFuel;
    }

    public double getTotalKmDriven() {
        return totalKmDriven;
    }

    public double getCurrentKmDriven() {
        return currentKmDriven;
    }

    public double getAverageFuelConsumption() {

        if (fuelConsumedMedian.isEmpty()) {
            return 0.0;
        }
        else {
            double sum = fuelConsumedMedian.stream()
                    .mapToDouble(a -> a)
                    .sum();
            averageFuelConsumption = sum / fuelConsumption.length;
            return averageFuelConsumption;
        }
    }

    public void setAvailableFuel(int availableFuel) {
        this.availableFuel = availableFuel;
    }

    public void setTireSize(int tireSize) {
        this.tireSize = tireSize;
    }
}
