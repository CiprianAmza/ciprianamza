package week4.domain;


public class MercedesBenz extends Mercedes {


    private static final double [] fuelConsumption = {5.6, 6.0, 6.5, 7.4, 7.9, 8.8};
    private int availableFuel;
    private final String fuelType = "Petrol";
    private int tireSize = 18; //default value
    private String chassisNumber;

    public MercedesBenz(int availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber, 18, "Petrol", fuelConsumption);
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }
}
