package week4.domain;

public class Logan extends Dacia {

    protected static final double [] fuelConsumption = {4.9, 5.1, 5.4, 6.5, 6.9, 7.3};
    protected int availableFuel;
    protected final String fuelType = "Diesel"; //Logan will use Diesel
    protected int tireSize = 15; //default value
    protected String chassisNumber;

    public Logan(int availableFuel, String chassisNumber) {
        super(availableFuel, chassisNumber, 15, "Diesel", fuelConsumption);
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
    }


}