package week8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import week8.domain.Competitor;

import static org.junit.jupiter.api.Assertions.*;

class CompetitorTest {
    Competitor competitor;

    @BeforeEach
    void setUp(){
        competitor = new Competitor("01", "John Marinescu", "ROU", "xxxxx xxxxx xooox", "19:30");
    }

    @Test
    @DisplayName("Checks if the penalties are added to the finish time")
    void getCompleteTime() {
        Competitor randomCompetitor = new Competitor("01", "Ciprian A", "ROU", "xxxxx xxxxx xxxxx", "20:00");

        // if no penalty, time should remain unchanged
        int timeBeforePenaltyAdded = randomCompetitor.transformTimeInSeconds(randomCompetitor.getTime());
        int timeAfterPenaltyAdded  = randomCompetitor.getCompleteTime();
        assertEquals(timeAfterPenaltyAdded, timeBeforePenaltyAdded, "Should work if there is no penalty");

        // if there are n penalties, the time must be bigger with 10 * n than the time before penalty
        Competitor anotherCompetitor = new Competitor("02", "Ciprian B", "ROU", "xxoox xooxx xxoxx", "54:12");
        timeBeforePenaltyAdded = anotherCompetitor.transformTimeInSeconds(anotherCompetitor.getTime());
        timeAfterPenaltyAdded = anotherCompetitor.getCompleteTime();
        assertNotEquals(timeAfterPenaltyAdded, timeBeforePenaltyAdded, "Checks that penalty is not ignored");
        assertEquals(timeAfterPenaltyAdded, timeBeforePenaltyAdded + 10 * 5, "Should work for any number of penalties"); // 5 penalties, each of 10 seconds worth
    }

    @Test
    @DisplayName("Should transform the time into an HH:MM:SS format")
    void transformTimeToStringFormat() {

        int currentTime = competitor.getCompleteTime(); // 1200 seconds
        String expectedValue = "0:" + Integer.toString(currentTime / 60) + ":0";
        String currentTimeAsString = competitor.transformTimeToStringFormat();

        assertEquals(expectedValue, currentTimeAsString, "Should transform seconds in time format HH:MM:SS");

    }

    @Test
    @DisplayName("Should get the first name only")
    void getFirstName() {
        String name = "Max Mustermann";
        Competitor competitor = new Competitor("1", name, "DE", "xxxxx xxxxx xxxxx", "10:00");
        assertEquals("Max", competitor.getFirstName(), "Should return only the first name");
    }

    @Test
    @DisplayName("Should get the last name only")
    void getLastName() {
        String name = "Max Mustermann";
        Competitor competitor = new Competitor("1", name, "DE", "xxxxx xxxxx xxxxx", "10:00");
        assertEquals("Mustermann", competitor.getLastName(), "Should return only the last name");
    }

    @Test
    @DisplayName("Should return the initial time, before adding penalties as Strng")
    void getTime() {
        String initialTime = competitor.getTime();
        String expectedTime = "19:30";
        String timeAfterPenalties = competitor.getTotalTime();

        assertEquals(initialTime, expectedTime);
        assertNotEquals(initialTime, timeAfterPenalties);
    }

    @Test
    @DisplayName("Should return the time after adding the penalties as String")
    void getTotalTime() {
        String timeBeforePenalties = competitor.getTime();
        String expectedTime = "0:20:0";
        String timeAfterPenalties = competitor.getTotalTime();

        assertEquals(timeAfterPenalties, expectedTime);
        assertNotEquals(timeBeforePenalties, timeAfterPenalties);
    }

    @Test
    @DisplayName("Should return the correct nationality")
    void getNationality() {
        Competitor englishMan = new Competitor("0", "S S", "UK", "xxxxx xxxxx xxxxx", "20:00");
        Competitor.Nationality expectedNationality = Competitor.Nationality.UK;
        Competitor.Nationality currentNationality = englishMan.getNationality();

        assertEquals(expectedNationality, currentNationality);
        assertNotEquals(Competitor.Nationality.CZ, currentNationality);
    }

    @Test
    @DisplayName("Should return the total penalty time")
    void getTotalPenaltyTime() {
        Competitor c = new Competitor("0", "A A", "DE", "xxxxx ooooo xxxxx", "13:33");
        int expectedPenaltyTime = 50;
        int penaltyTime = c.getTotalPenaltyTime();

        assertEquals(expectedPenaltyTime, penaltyTime);
    }

    @Test
    @DisplayName("Should return the right id")
    void getID() {
        Competitor someName = new Competitor("0", "Some Name", "ROU", "xxxxx xxxxx xxxxx", "23:33");
        int expectedID = 0;
        int getId = someName.getID();
        assertEquals(getId, expectedID, "Should parse the string id into integer");
    }
}