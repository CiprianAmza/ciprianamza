package week6.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CalculatorTest {

    @Mock
    private Calculator myCalculator;

    @BeforeEach
    void setUp(){
        myCalculator = new Calculator();
    }

    @org.junit.jupiter.api.Test
    void calculateTheCurrentExpressionWithGivenUnit_equals_desiredUnit() {
        Calculator anotherCalculator = Mockito.mock(Calculator.class);
        Mockito.when(anotherCalculator.calculate("10m+10m", "m")).thenReturn(20.0);

        double answer = myCalculator.calculate("10 m +   1 0   m", "m");

        assertEquals(answer, anotherCalculator.calculate("10m+10m", "m"));
    }

    @org.junit.jupiter.api.Test
    void calculateTheCurrentExpression_equals_desiredValue() {

        for (int i = 1; i < 100; i++) {
            String value = String.valueOf(i) + "mm";
            value += "+" + value;
            System.out.println(value + " = " + (i+i) + " mm");
            assertEquals(i+i, myCalculator.calculate(value));
        }

        for (int i = 1; i < 100; i++) {
            String value = String.valueOf(i) + "cm";
            value += "+" + value;
            System.out.println(value + " = " + (i+i) + " cm");
            assertEquals((i+i) * 10, myCalculator.calculate(value));
        }

        for (int i = 1; i < 100; i++) {
            String value = String.valueOf(i) + "cm";
            value += "-" + value;
            System.out.println(value + " = " + (i-i) + " cm");
            assertEquals(0.0, myCalculator.calculate(value));
        }


    }

    @org.junit.jupiter.api.Test
    void addToCurrentValue_equals_newValue() {

        myCalculator.setValue(10);
        assertEquals(10.0, myCalculator.getCurrentValue());

        myCalculator.add(5);
        assertEquals(15.0, myCalculator.getCurrentValue());
    }

    @org.junit.jupiter.api.Test
    void clearValue_equals_Zero() {

        myCalculator.setValue(100);
        myCalculator.clearValue();
        double currentValue = myCalculator.getCurrentValue();
        assertEquals(0, currentValue);

    }

    @org.junit.jupiter.api.Test
    void getCurrentValue_equals_currentValue() {

        Calculator anotherCalculator = Mockito.mock(Calculator.class);
        Mockito.when(anotherCalculator.getCurrentValue()).thenReturn(100.0);
        myCalculator.add(100.0);

        double testedValue = anotherCalculator.getCurrentValue() - myCalculator.getCurrentValue();
        assertEquals(0, testedValue);
    }
}